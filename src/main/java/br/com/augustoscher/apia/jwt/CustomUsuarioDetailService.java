package  br.com.augustoscher.apia.jwt;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.augustoscher.apia.helper.DataHelper;
import br.com.augustoscher.apia.models.Usuario;

@Component
public class CustomUsuarioDetailService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Usuario user = DataHelper.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("Usuário não encontrado");
        }
		
        List<GrantedAuthority> authorityListAdmin = getAuthorityListAdmin();
        List<GrantedAuthority> authorityListUsers = getAuthorityListUsers();
		return new User(user.getLogin(), user.getSenha(), user.isAdmin() ? authorityListAdmin : authorityListUsers);
	}

    private List<GrantedAuthority> getAuthorityListAdmin() {
        return AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN");
    }

    private List<GrantedAuthority> getAuthorityListUsers() {
        return AuthorityUtils.createAuthorityList("ROLE_USER");
    }

}
