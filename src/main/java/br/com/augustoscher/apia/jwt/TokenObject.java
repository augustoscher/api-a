package br.com.augustoscher.apia.jwt;

public class TokenObject {
	
	private  String access_token;
	
	public TokenObject(String tokrn) {
		this.access_token = tokrn;
	}


	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

}
