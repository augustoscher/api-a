package br.com.augustoscher.apia.jwt;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoder {

	public static void main(String[] args) {
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
        System.out.println(bCrypt.encode("teste"));
	}
	
	public static String encode(String str) {
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		return bCrypt.encode(str);
	}
}
