package br.com.augustoscher.apia.jwt;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.augustoscher.apia.models.Usuario;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;
	
	public JWTAuthenticationFilter(AuthenticationManager authentication) {
		this.authenticationManager = authentication;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		try {
            Usuario user = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);
            Authentication auth = this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getLogin(), user.getSenha()));
            return auth;
		} catch (IOException e) {
			throw new RuntimeException(e); 
		}
	}
	
    /**
     * Geração de token com base no username
     */
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		//gerar token com username
        User user = ((User) authResult.getPrincipal());
		String token = Jwts
				.builder()//
                .setSubject(user.getUsername())//
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))//
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
				.compact();
		
		String bearerToken = SecurityConstants.TOKEN_PREFIX + token;
        String jsonString = new Gson().toJson(new TokenObject(bearerToken));
		
        response.getWriter().write(jsonString);
		response.addHeader(SecurityConstants.HEADER_STRING, bearerToken); //adiciona o token no header da requisição.
	}

}
