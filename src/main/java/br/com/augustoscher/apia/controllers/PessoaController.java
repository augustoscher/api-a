package br.com.augustoscher.apia.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.augustoscher.apia.helper.DataHelper;

@RestController
@RequestMapping("v1/pessoas")
public class PessoaController {

    @GetMapping
    public ResponseEntity<?> get() {
        return new ResponseEntity<>(DataHelper.pessoas, HttpStatus.OK);
    }
}
