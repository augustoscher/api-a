package br.com.augustoscher.apia.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.augustoscher.apia.helper.DataHelper;

@RestController
@RequestMapping("v1/dividas")
public class DividaController {

    @GetMapping
    public ResponseEntity<?> get() {
        return new ResponseEntity<>(DataHelper.dividas, HttpStatus.OK);
    }

    @GetMapping(path = "/{cpf}")
    public ResponseEntity<?> getPorUuId(@PathVariable("cpf") String cpf, Authentication auth) {
        return new ResponseEntity<>(DataHelper.findDividasByCpf(cpf), HttpStatus.OK);
    }
}
