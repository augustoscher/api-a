package br.com.augustoscher.apia.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import br.com.augustoscher.apia.models.Divida;
import br.com.augustoscher.apia.models.Endereco;
import br.com.augustoscher.apia.models.Instituicao;
import br.com.augustoscher.apia.models.Pessoa;
import br.com.augustoscher.apia.models.StatusDivida;
import br.com.augustoscher.apia.models.TipoDivida;
import br.com.augustoscher.apia.models.Usuario;

public class DataHelper {

    public static List<Divida> dividas = new ArrayList<>();
    public static List<Pessoa> pessoas = new ArrayList<>();
    public static List<Instituicao> instituicoes = new ArrayList<>();
    public static List<Usuario> usuarios = new ArrayList<>();

    public static void init() {
        Instituicao instA = getInstituicao("InstA", "Fantasia-A", "6865146546864");
        Instituicao instB = getInstituicao("InstB", "Fantasia-B", "6865146546864");
        instituicoes.add(instA);
        instituicoes.add(instB);

        Pessoa pessoaA = getPessoa("Kono", "896754658646", instA, instB);
        Pessoa pessoaB = getPessoa("Mako", "659846546887", instA, instB);
        pessoas.add(pessoaA);
        pessoas.add(pessoaB);

        pessoas.forEach(pessoa -> {
            pessoa.getDividas().forEach(divida -> {
                dividas.add(divida);
            });
        });
        System.out.println(dividas.size());
        usuarios = getUsers();
    }

    static List<Usuario> getUsers() {
        Usuario user = new Usuario();
        user.setLogin("augusto");
        user.setAdmin(true);
        user.setSenha("$2a$10$Hhrk4ap2.9.GGwz6csgKhOon02MAOPp/0VjZ75meIyCoh8KMOs6ZG");//testing

        Usuario user2 = new Usuario();
        user2.setLogin("teste");
        user2.setAdmin(false);
        user2.setSenha("$2a$10$3MUfkam8gaTOJreCxPM1wuC7sWyjjnag/RngNztL8icS8AfPoZZm6");//teste
        List<Usuario> result = new ArrayList<>();
        result.add(user);
        result.add(user2);
        return result;
    }

    static Pessoa getPessoa(String nome, String cpf, Instituicao instA, Instituicao instB) {
        Pessoa p = new Pessoa();
        p.setUuid(UUID.randomUUID());
        p.setNome(nome);
        p.setCpf(cpf);
        p.getEnderecos().add(getEndereco());
        p.getDividas().addAll(getListaDividas(instA, instB));
        p.getDividas().forEach(item -> {
            item.setDevedor(p);
        });
        return p;
    }

    static Endereco getEndereco() {
        Endereco e = new Endereco();
        e.setUuid(UUID.randomUUID());
        e.setRua("Wilhelm Budag");
        e.setNumero("84");
        e.setCep("89045090");
        e.setCidade("Florianópolis");
        e.setEstado("SC");
        e.setPais("Brasil");
        return e;
    }

    static Instituicao getInstituicao(String razao, String fantasia, String cnpj) {
        Instituicao i = new Instituicao();
        i.setUuid(UUID.randomUUID());
        i.setRazaoSocial(razao);
        i.setFantasia(fantasia);
        i.setCnpj(cnpj);
        i.getEnderecos().add(getEndereco());
        return i;
    }

    static List<Divida> getListaDividas(Instituicao instA, Instituicao instB) {
        List<Divida> result = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            BigDecimal valor = new BigDecimal(i * 10);
            Divida d = getDivida(valor);
            if (i >= 5) {
                d.setInstituicao(instB);
                d.setTipo(TipoDivida.EMPRESTIMO);
                d.setStatus(StatusDivida.ABERTO);
            } else {
                d.setInstituicao(instA);
                d.setTipo(TipoDivida.COBRANCA_JUDICIAL);
                d.setStatus(StatusDivida.QUITADA);
            }
            result.add(d);
        }

        return result;
    }

    static Divida getDivida(BigDecimal valor) {
        Divida d = new Divida();
        d.setUuid(UUID.randomUUID());
        d.setValor(valor);
        d.setData(new Date());
        d.setDataQuitacao(new Date());
        return d;
    }

    public static Usuario findByLogin(String login) {
        for (Usuario u : usuarios) {
            if (u.getLogin().equalsIgnoreCase(login)) {
                return u;
            }
        }
        return null;
    }

    public static List<Divida> findDividasByCpf(String cpf) {
        List<Divida> result = new ArrayList<>();
        dividas.forEach(divida -> {
            if (divida.getDevedor().getCpf().equalsIgnoreCase(cpf)) {
                result.add(divida);
            }
        });
        return result;
    }

}
