package br.com.augustoscher.apia;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.augustoscher.apia.helper.DataHelper;

@SpringBootApplication
public class ApiA {
	
	public static void main(String[] args) {
        SpringApplication.run(ApiA.class);
	}

    @Bean
    public CommandLineRunner demo() {
        return (args) -> {
            DataHelper.init();
        };
    }
}
