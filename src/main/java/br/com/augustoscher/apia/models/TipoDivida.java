package br.com.augustoscher.apia.models;


public enum TipoDivida {

    EMPRESTIMO(0, "Empréstimo"), 
    AQUISICAO(1, "Aquisição"),
    COBRANCA_JUDICIAL(2, "Cobrança Judicial");
    
    private int id;
    private String descricao;

    TipoDivida(int id, String desc) {
        this.id = id;
        this.descricao = desc;
    }

    public int getId() {
        return this.id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public static TipoDivida fromValue(int value) {
        for (TipoDivida tipo : values()) {
            if (tipo.getId() == value) {
                return tipo;
            }
        }
        throw new IllegalArgumentException("value not valid");
    }

    public String toJson() {
        return this.id + " - " + this.descricao;
    }
}
