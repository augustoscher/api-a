package br.com.augustoscher.apia.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

public class Divida {

    private UUID uuid;
    private Pessoa devedor;
    private BigDecimal valor;
    private Date data;
    private Date dataQuitacao;
    private TipoDivida tipo;
    private StatusDivida status;
    private Instituicao instituicao;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Pessoa getDevedor() {
        return devedor;
    }

    public void setDevedor(Pessoa devedor) {
        this.devedor = devedor;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getDataQuitacao() {
        return dataQuitacao;
    }

    public void setDataQuitacao(Date dataQuitacao) {
        this.dataQuitacao = dataQuitacao;
    }

    public TipoDivida getTipo() {
        return tipo;
    }

    public void setTipo(TipoDivida tipo) {
        this.tipo = tipo;
    }

    public StatusDivida getStatus() {
        return status;
    }

    public void setStatus(StatusDivida status) {
        this.status = status;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

}
