package br.com.augustoscher.apia.models;


public enum StatusDivida {

    ABERTO(0, "Aberta"), 
    QUITADA(1, "Quitada"),
    RENEGOCIADA(2, "Renegociada");
    
    private int id;
    private String descricao;

    StatusDivida(int id, String desc) {
        this.id = id;
        this.descricao = desc;
    }

    public int getId() {
        return this.id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public static StatusDivida fromValue(int value) {
        for (StatusDivida tipo : values()) {
            if (tipo.getId() == value) {
                return tipo;
            }
        }
        throw new IllegalArgumentException("value not valid");
    }

    public String toJson() {
        return this.id + " - " + this.descricao;
    }
}
