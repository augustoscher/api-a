# api-a

### Pré-Requisitos  
1) Ter Java instalado  
2) Ter ferramenta para testes de endpoints (Postman, Curl).  

### Passos para iniciar a aplicação  
1) Clonar o projeto em seu workspace.    
git clone https://augustoscher@bitbucket.org/augustoscher/api-a.git  

2) Entrar no diretorio target dentro do diretorio clonado  
cd api-a/target  

3) Executar o comando que inicializa a api  
java -jar api-a-0.0.1-SNAPSHOT.jar

### Passos para executar testes
Através do Postman/Curl  

1) Fazer login na api: 
executar chamada POST em http://localhost:3001/login  
body da requisição:  
{"login": "augusto", "senha": "testing"}
    
será retornado um JWT, por exemplo:  
{
    "access_token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhdWd1c3RvIiwiZXhwIjoxNTQ4NzE0OTYyfQ.qr5sBv50E8uZ4NosqGtyLBC0siw882mPJrYvy5HXpqtFnpaaYSOCPhh6VtGP3m89yc4Dehk9cKutqnfsmQKq-Q"
}  
  
2) Buscar dívidas com base em um cpf:  
adicionar o Bearer Token no header da requisição através do postman.  
executar chamada GET http://localhost:3001/v1/dividas/659846546887  
será retornada a lista de dívidas com base no cpf informado.  
A url http://localhost:3001/v1/dividas retorna todas as dívidas existentes.  





